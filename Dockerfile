ARG NGINX_VERSION=1.20.0

FROM nginx:${NGINX_VERSION}-alpine

LABEL MAINTAINER="DevTheJo <devthejo@protonmail.com>"

EXPOSE 8087
ENV NGINX_VERSION=$NGINX_VERSION \
  DOCKERIZE_VERSION=v0.6.1 \
  PORT=8087 \
  FORWARD_HOST=localhost \
  FORWARD_PORT=8080 \
  BASIC_AUTH_USERNAME_FILE= \
  BASIC_AUTH_PASSWORD_FILE=/vault/secrets/password \
  BASIC_AUTH_USERNAME=admin \
  BASIC_AUTH_PASSWORD= \
  PROXY_READ_TIMEOUT=60s \
  PROXY_SEND_TIMEOUT=60s \
  CLIENT_MAX_BODY_SIZE=1m \
  PROXY_REQUEST_BUFFERING=on \
  PROXY_BUFFERING=on

RUN wget -O dockerize.tar.gz https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && tar -C /usr/local/bin -xzvf dockerize.tar.gz \
  && apk add --update --no-cache --virtual entrypoint apache2-utils inotify-tools bash \
  && rm dockerize.tar.gz /etc/nginx/conf.d/default.conf /etc/nginx/nginx.conf \
  && mkdir /templates \
  && chmod g+rw /etc/nginx /etc/nginx/conf.d /templates

COPY default.conf.tpl nginx.conf.tpl /templates/
COPY run-nginx.sh /usr/local/bin/

CMD ["run-nginx.sh"]
